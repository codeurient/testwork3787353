-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 22, 2022 at 11:40 AM
-- Server version: 8.0.24
-- PHP Version: 8.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testwork3787353`
--

-- --------------------------------------------------------

--
-- Table structure for table `cinemas`
--

CREATE TABLE `cinemas` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cinemas`
--

INSERT INTO `cinemas` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'babmuk', '2022-04-22 01:38:58', '2022-04-22 01:38:58'),
(2, 'ex', '2022-04-22 01:38:58', '2022-04-22 01:38:58'),
(3, 'dolore', '2022-04-22 01:38:58', '2022-04-22 01:38:58'),
(4, 'numquam', '2022-04-22 01:38:58', '2022-04-22 01:38:58'),
(5, 'eos', '2022-04-22 01:38:58', '2022-04-22 01:38:58'),
(6, 'xerson', '2022-04-22 01:38:58', '2022-04-22 01:38:58'),
(7, 'neque', '2022-04-22 01:38:58', '2022-04-22 01:38:58'),
(8, 'ducimus', '2022-04-22 01:38:58', '2022-04-22 01:38:58');

-- --------------------------------------------------------

--
-- Table structure for table `cinema_movie`
--

CREATE TABLE `cinema_movie` (
  `id` bigint UNSIGNED NOT NULL,
  `cinema_id` bigint UNSIGNED NOT NULL,
  `movie_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_finished` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cinema_movie`
--

INSERT INTO `cinema_movie` (`id`, `cinema_id`, `movie_id`, `created_at`, `updated_at`, `is_finished`) VALUES
(1, 6, 3, '2022-04-22 01:39:00', '2022-04-22 01:39:00', 0),
(2, 6, 7, '2022-04-22 01:39:00', '2022-04-22 01:39:00', 0),
(3, 5, 2, '2022-04-22 01:39:00', '2022-04-22 01:39:00', 0),
(4, 8, 21, '2022-04-22 01:39:00', '2022-04-22 01:39:00', 0),
(5, 3, 19, '2022-04-22 01:39:00', '2022-04-22 01:39:00', 0),
(6, 4, 15, '2022-04-22 01:39:00', '2022-04-22 01:39:00', 1),
(7, 2, 29, '2022-04-22 01:39:00', '2022-04-22 01:39:00', 0),
(8, 7, 29, '2022-04-22 01:39:00', '2022-04-22 01:39:00', 1),
(9, 4, 14, '2022-04-22 01:39:00', '2022-04-22 01:39:00', 1),
(10, 7, 16, '2022-04-22 01:39:00', '2022-04-22 01:39:00', 0),
(11, 5, 19, '2022-04-22 01:39:00', '2022-04-22 01:39:00', 0),
(12, 2, 4, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 1),
(13, 4, 8, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 0),
(14, 4, 10, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 1),
(15, 6, 20, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 1),
(16, 3, 21, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 0),
(17, 8, 14, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 1),
(18, 8, 15, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 1),
(19, 1, 2, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 0),
(20, 5, 3, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 0),
(21, 2, 1, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 0),
(22, 4, 7, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 0),
(23, 7, 15, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 1),
(24, 1, 26, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 0),
(25, 3, 19, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 1),
(26, 6, 11, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 0),
(27, 2, 1, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 0),
(28, 5, 15, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 0),
(29, 4, 15, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 1),
(30, 1, 11, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 1),
(31, 6, 18, '2022-04-22 01:39:01', '2022-04-22 01:39:01', 1),
(32, 3, 25, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 1),
(33, 7, 12, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 0),
(34, 5, 28, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 1),
(35, 8, 24, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 1),
(36, 6, 3, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 1),
(37, 6, 26, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 0),
(38, 4, 12, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 0),
(39, 4, 19, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 0),
(40, 2, 21, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 0),
(41, 2, 13, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 0),
(42, 4, 20, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 0),
(43, 2, 13, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 1),
(44, 8, 3, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 0),
(45, 6, 5, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 1),
(46, 1, 9, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 0),
(47, 1, 25, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 0),
(48, 2, 15, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 0),
(49, 7, 12, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 0),
(50, 5, 7, '2022-04-22 01:39:02', '2022-04-22 01:39:02', 0);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_04_21_103629_create_cinemas_table', 1),
(6, '2022_04_21_103649_create_movies_table', 1),
(7, '2022_04_21_103915_create_cinema_movie_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'architecto', '2022-04-22 01:38:58', '2022-04-22 01:38:58'),
(2, 'nam', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(3, 'illo', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(4, 'rerum', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(5, 'vel', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(6, 'sit', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(7, 'nostrum', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(8, 'voluptatem', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(9, 'voluptas', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(10, 'explicabo', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(11, 'accusantium', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(12, 'soluta', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(13, 'eveniet', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(14, 'officia', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(15, 'ut', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(16, 'et', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(17, 'ea', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(18, 'perspiciatis', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(19, 'quam', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(20, 'aliquid', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(21, 'vel', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(22, 'repellendus', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(23, 'debitis', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(24, 'voluptatibus', '2022-04-22 01:38:59', '2022-04-22 01:38:59'),
(25, 'harry potter', '2022-04-22 01:39:00', '2022-04-22 01:39:00'),
(26, 'nisi', '2022-04-22 01:39:00', '2022-04-22 01:39:00'),
(27, 'quas', '2022-04-22 01:39:00', '2022-04-22 01:39:00'),
(28, 'voluptatibus', '2022-04-22 01:39:00', '2022-04-22 01:39:00'),
(29, 'aperiam', '2022-04-22 01:39:00', '2022-04-22 01:39:00'),
(30, 'veritatis', '2022-04-22 01:39:00', '2022-04-22 01:39:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test@gmail.com', NULL, '$2y$10$9AwmoLBbruCrGlvAzXxg0uiTYJPGbJMT1lk6GraBeNQSlMZ9VNWTi', NULL, '2022-04-22 01:39:38', '2022-04-22 01:39:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cinemas`
--
ALTER TABLE `cinemas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cinema_movie`
--
ALTER TABLE `cinema_movie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cinema_movie_cinema_id_foreign` (`cinema_id`),
  ADD KEY `cinema_movie_movie_id_foreign` (`movie_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cinemas`
--
ALTER TABLE `cinemas`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cinema_movie`
--
ALTER TABLE `cinema_movie`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cinema_movie`
--
ALTER TABLE `cinema_movie`
  ADD CONSTRAINT `cinema_movie_cinema_id_foreign` FOREIGN KEY (`cinema_id`) REFERENCES `cinemas` (`id`),
  ADD CONSTRAINT `cinema_movie_movie_id_foreign` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
