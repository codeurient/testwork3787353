<?php


namespace App\Repositories;


use App\Models\Cinema;
use Illuminate\Http\Request;

class CinemaRepository
{
    public function all(){
        return Cinema::query()->with('movies')->get()
            ->map(function ($cinema){
                return [
                    'cinema_id' => $cinema->id,
                    'cinema_name' => $cinema->name,
                    'movies' => $cinema->movies,

                ];
            });
    }

    public function sortAndSearch($request){
        $query = Cinema::query();
        if ($s = $request->input('s')) {
            $query->whereRaw("name LIKE '%" . $s . "%'")
                ->orWhereRaw("id LIKE '%" . $s . "%'");
        }
        if ($sort = $request->input('sort')){
            $query->orderBy('id', $sort);

        }
        return $query->with('movies')->get();
    }
}
