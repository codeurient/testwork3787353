<?php

namespace App\Http\Controllers;

use App\Models\Cinema;
use App\Models\Movie;
use App\Repositories\CinemaRepository;
use Illuminate\Http\Request;

class CinemaController extends Controller
{
    /**
     * @var CinemaRepository
     */
    private $cinemaRepository;

    public function __construct(CinemaRepository $cinemaRepository)
    {
        $this->cinemaRepository = $cinemaRepository;
    }

    public function index(){
        return $cinemas = $this->cinemaRepository->all();
    }

    public function show(Cinema $cinema){
        return $cinema->load('movies');
    }

    public function search(Request $request){
        return $sortAndSearch = $this->cinemaRepository->sortAndSearch($request);
    }
}
