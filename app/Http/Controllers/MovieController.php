<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function index(){
        return Movie::query()->with('cinemas')->get();
    }

    public function show(Movie $movie){
        return $movie->load('cinemas');
    }
}
