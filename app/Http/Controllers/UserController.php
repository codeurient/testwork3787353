<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request, $id){
        $user = User::query()->find($id);
        if (!$user) return response('', 404);
        return $user;
    }
}
