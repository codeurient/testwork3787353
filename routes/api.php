<?php

use App\Http\Controllers\CinemaController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;




//Route::middleware('auth_api')->match(['post', 'get'], '/user/{id}', [UserController::class, 'index']);
Route::middleware('auth_api')->get('/user/{id}', [UserController::class, 'index']);

Route::resource('cinemas', CinemaController::class);
Route::get('/search', [CinemaController::class, 'search'])->name('cinemas.search');

Route::resource('movies', MovieController::class);

